package com.example.romik9415.itquest20;

import java.net.Socket;

public class Client {


    private Integer place;
    private String name;
    private Integer score;

    public void setPlace(Integer place) {
        this.place = place;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getPlace() {
        return place;
    }

    public String getName() {
        return name;
    }

    public Integer getScore() {
        return score;
    }

    public Client(Integer place) {
        this.place = place;
    }

    class Session extends Thread {
            Socket socket; // отримання сокет сесії
            public Session(Socket s) {socket = s;}
            public void run() {
                }
            }
}
