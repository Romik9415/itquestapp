package com.example.romik9415.itquest20;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EdgeEffect;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {
    Button loginButton;
    EditText login;
    EditText password;
    Boolean auth = false;
    Integer teamNamber;
    SharedPreferences sharedPreferences;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mPasswordsRef = database.getReference("auth");

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        if(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("LogIn",false)){
            startActivity(new Intent(this,MainActivity.class));
            finish();
        }
        setContentView(R.layout.activity_login);
        setTitle("Registration");
        loginButton = (Button) findViewById(R.id.login_button);
        login = (EditText) findViewById(R.id.login);
        password = (EditText) findViewById(R.id.password);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //checkTeam(login,password);
                checkTeam( login, password);
            }
        });
    }

    private boolean checkTeam(final EditText login, final EditText password) {
        mPasswordsRef.child(String.valueOf(login.getText())).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("password", String.valueOf(dataSnapshot.getValue()));
                if(dataSnapshot.getValue()!=null) {
                    //Toast.makeText(LoginActivity.this, "Not correct login", Toast.LENGTH_SHORT).show();

                    Log.d("password", String.valueOf(dataSnapshot.getValue()));
                    Log.d("password", String.valueOf(password.getText()));
                    if (password.getText().length() != 0) {
                        if (dataSnapshot.getValue().equals(password.getText().toString())) {
                            auth = true;

                            PreferenceManager.getDefaultSharedPreferences(LoginActivity.this).edit().putBoolean("LogIn", true).apply();
                            switch (String.valueOf(login.getText())){
                                case "Comanda_A": teamNamber=1; break;
                                case "CS22": teamNamber =2;break;
                                case "GD5":teamNamber=3;break;
                                case "PZshniki": teamNamber=4;break;
                            }
                            sharedPreferences.edit().putInt("teamNumber", teamNamber).apply();
                            sharedPreferences.edit().putString("teamName",String.valueOf(login.getText())).apply();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));

                        } else {
                            auth = false;

                            Toast.makeText(LoginActivity.this, "Invalid password", Toast.LENGTH_SHORT).show();
                        }

                    } else {
                        Toast.makeText(LoginActivity.this, "Enter password", Toast.LENGTH_SHORT).show();
                    }
                }
                else {
                    Toast.makeText(LoginActivity.this, "Invalid login", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return auth;
    }

}
