package com.example.romik9415.itquest20;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewTreeObserver;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseIndexRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private static final int MY_CAMERA_REQUEST_CODE = 100;
    public Boolean status;
    ArrayList<Integer> frozennInt;
    RecyclerView firebaseRecycleView;
    FirebaseRecyclerAdapter<Team,ViewHolder> firebaseRecyclerAdapter;
    TextView navTeamName;
    SharedPreferences sharedPreferences;

    FirebaseDatabase database = FirebaseDatabase.getInstance();
    DatabaseReference mMessageRef = database.getReference("message");
    Query mTeamsRef = database.getReference("teams").orderByChild("score");
    DatabaseReference mStatus = database.getReference("status");
    LinearLayoutManager linearLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//
//        if(!PreferenceManager.getDefaultSharedPreferences(this).getBoolean("LogIn",false)){
//            startActivity(new Intent(this,LoginActivity.class));
//            // finish();
//        }
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!(ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA)
                        == PackageManager.PERMISSION_DENIED)){
                    startActivity(new Intent(MainActivity.this, QRScaner.class));
                }
                else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[] {Manifest.permission.CAMERA}, MY_CAMERA_REQUEST_CODE);
                }
            }
        });
        firebaseRecycleView = (RecyclerView) findViewById(R.id.firebseRecycleView);
        linearLayoutManager = new LinearLayoutManager(this);
        firebaseRecyclerAdapter = new FirebaseIndexRecyclerAdapter<Team, ViewHolder>(
                Team.class,
                R.layout.team_item_for_adapter,
                ViewHolder.class,
                mTeamsRef,
                mTeamsRef
        ) {
            @Override
            protected void populateViewHolder(ViewHolder viewHolder, Team model, int position) {
                if(status) {
                    viewHolder.place.setText(String.valueOf(4 - position));
                    viewHolder.teamName.setText(model.getName());
                    viewHolder.score.setText(String.valueOf(model.getScore()));
                }
            }
        };

        firebaseRecycleView.setLayoutManager(linearLayoutManager);
        firebaseRecycleView.setAdapter(firebaseRecyclerAdapter);


        mTeamsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                final Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (status) {
                            firebaseRecyclerAdapter.notifyDataSetChanged();
                        }
                    }
                }, 1000);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View header=navigationView.getHeaderView(0);
        navTeamName = (TextView) header.findViewById(R.id.navigation_team_name);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        navTeamName.setText(sharedPreferences.getString("teamName", "NOname"));


    }


    @Override
    protected void onStart() {
        super.onStart();

        mStatus.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if(dataSnapshot.getValue(Boolean.class)){
                    status = true;
                    Log.d("status","true");

                }else {
                    status = false;
                    Log.d("status","false");
                    Toast.makeText(MainActivity.this, "Table is frozen ", Toast.LENGTH_SHORT).show();
                };
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mMessageRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
                String value = dataSnapshot.getValue(String.class);
                Log.d("Firebase", "Value is: " + value);
                if(!value.equals("")) {
                    Toast.makeText(MainActivity.this, "Message: " + value, Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w("Firebase", "Failed to read value.", error.toException());
            }
        });

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.company1) {
            // Handle the camera action
//        } else if (id == R.id.company2) {
//
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        firebaseRecyclerAdapter.cleanup();
        //mMessageRef.removeEventListener(va);
    }

}

