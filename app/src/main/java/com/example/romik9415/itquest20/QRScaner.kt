package com.example.romik9415.itquest20

import android.app.Activity
import android.content.SharedPreferences
import android.preference.PreferenceManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast

import com.google.zxing.Result

import java.io.BufferedInputStream
import java.io.IOException
import java.io.InputStream
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URL

import me.dm7.barcodescanner.zxing.ZXingScannerView

class QRScaner : Activity(), ZXingScannerView.ResultHandler {
    private var mScannerView: ZXingScannerView? = null
    lateinit  var  sharedPreferences :SharedPreferences

    public override fun onCreate(state: Bundle?) {
        super.onCreate(state)
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        mScannerView = ZXingScannerView(this)   // Programmatically initialize the scanner view
        setContentView(mScannerView)                // Set the scanner view as the content view
        sharedPreferences
    }

    public override fun onResume() {
        super.onResume()
        mScannerView!!.setResultHandler(this) // Register ourselves as a handler for scan results.
        mScannerView!!.startCamera()          // Start camera on resume
        Log.d("QrScaner", "i`m resume")
    }

    public override fun onPause() {
        super.onPause()
        mScannerView!!.stopCamera()           // Stop camera on pause
        Log.d("QrScaner", "i`m pause")
    }

    override fun handleResult(rawResult: Result) {
        // Do something with the result here
        if (rawResult.barcodeFormat.toString() == "QR_CODE") {
            val urlString = "https://us-central1-it-quest.cloudfunctions.net/addRecord?tId=" + sharedPreferences.getInt("teamNumber", 0) + rawResult.text
            Log.d("C_QS", "urlConnection")
            Thread(Runnable {
                var url: URL? = null

                try {
                    url = URL(urlString)
                    val urlConnection = url.openConnection() as HttpURLConnection
                    Log.d("C_QS", "urlConnection opened")
                    try {
                        val `in` = BufferedInputStream(urlConnection.inputStream)
                        finish()

                    } catch (e: IOException) {
                        e.printStackTrace()
                        Log.d("C_QS", "urlConnection opened")
                    } finally {
                        Log.d("C_QS", "urlConnection opened")
                        urlConnection.disconnect()
                    }

                } catch (e: IOException) {
                    Log.d("C_QS", "urlConnection error" + e.message)

                    e.printStackTrace()
                }
            }).start()

        }
        Log.v("QR", rawResult.text) // Prints scan results
        Log.v("QR", rawResult.barcodeFormat.toString()) // Prints the scan format (qrcode, pdf417 etc.)
        val server = Server()
        server.name
        //https://us-central1-it-quest.cloudfunctions.net/addRecord?tId=1&cId=2&status=1
    }


}
