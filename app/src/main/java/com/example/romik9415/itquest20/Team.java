package com.example.romik9415.itquest20;

public class Team {
    private Integer place;
    private String name;
    private Integer score;

    public void setPlace(Integer place) {
        this.place = place;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getPlace() {
        return place;
    }

    public String getName() {
        return name;
    }

    public Integer getScore() {
        return score;
    }

    public Team(Integer place, String name, Integer score) {
        this.place = place;
        this.name = name;
        this.score = score;
    }

    public Team(){}

};
