package com.example.romik9415.itquest20;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

public class ViewHolder extends RecyclerView.ViewHolder{

    TextView place;
    TextView teamName;
    TextView score;

    public ViewHolder(View itemView) {
        super(itemView);
         place = (TextView) itemView.findViewById(R.id.place);
        teamName = (TextView) itemView.findViewById(R.id.team_name);
        score = (TextView) itemView.findViewById(R.id.score);
    }
}
